//
//  CharacterDetailsViewController.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 24/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit
import CoreData

let ADD_FAVORITE = "ADD TO FAVORITES"
let REMOVE_FAVORITE = "REMOVE FROM FAVORITES"

class CharacterDetailsViewController: UIViewController {
    
    //MARK: - Properties
    var character:Character!
    var openDetailFrom:OpenDetailFrom!
    
    private var appDelegate:AppDelegate!
    private var managedcontext: NSManagedObjectContext!
    private let charactersTask = CharactersTask()
    
    //MARK: - @IBOutlets
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var imgHero: UIImageView!
    @IBOutlet weak var txvDetails: UITextView!
    @IBOutlet weak var lblHeroName: UILabel!
    @IBOutlet weak var btnFavoritesOption: UIButton!
    
    //MARK: - Enums
    enum OpenDetailFrom {
        case Favorites
        case Chacteres
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setDetailValues()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedcontext = appDelegate.managedObjectContext
        
        self.charactersTask.checkCharacterIsFavoriteById(self.character.id!) { (isFavorite) in
            if(isFavorite == false){
                self.btnFavoritesOption.setTitle(ADD_FAVORITE, forState: .Normal)
            }else{
                self.btnFavoritesOption.setTitle(REMOVE_FAVORITE, forState: .Normal)
            }
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Functions
    func setDetailValues(){
        
        Util.loadImageFromStringUrl(self.imgHero, strUrl: character.thumbnail!)
        
        self.imgHero.contentMode = .Center
        
        if(character.description_detail != ""){
            self.txvDetails.text = character.description_detail
        }
        
        self.navItem.title = character.name!.uppercaseString
        self.lblHeroName.text = character.name!.uppercaseString
        
    }
    
    //MARK: - @IBActions
    @IBAction func actionFavoriteOption(sender: AnyObject) {
        
        self.btnFavoritesOption.enabled = false
        
        self.charactersTask.checkCharacterIsFavoriteById(self.character.id!) { (isFavorite) in
            if(isFavorite == false){
                
                self.charactersTask.addCharacterToFavorites(self.character, completion: { (status) in
                    if(status == true){
                        self.character.isFavorite = true
                        dispatch_async(dispatch_get_main_queue(), {
                        self.btnFavoritesOption.setTitle(REMOVE_FAVORITE, forState: .Normal)
                        })
                    }
                    self.btnFavoritesOption.enabled = true
                })
                
            }else{
                
                self.charactersTask.removeCharacterFromFavoritesByCharacterId(self.character.id!, completion: { (status) in
                    if(status == true){
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            self.btnFavoritesOption.setTitle(ADD_FAVORITE, forState: .Normal)
                        })
                        
                        if(self.openDetailFrom == CharacterDetailsViewController.OpenDetailFrom.Favorites){
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        self.character.isFavorite = false
                    }
                    self.btnFavoritesOption.enabled = true
                })
            }
        }
        
    }
    
    @IBAction func actionOpenHeroEvents(sender: AnyObject) {
        self.performSegueWithIdentifier("segueEvents", sender: nil)
    }
    
    //MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "segueEvents"){
            let eventsViewController = segue.destinationViewController as! EventsViewController
            eventsViewController.characterId = self.character.id!
            eventsViewController.openEventsFrom = self.openDetailFrom
            
        }
    }
    
}
