//
//  EventsCollectionViewCell.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 26/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit

class EventsCollectionViewCell: UICollectionViewCell {
    
    //MARK: - @IBOutlets
    @IBOutlet weak var imgPicture: UIImageView!
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Functions
    func configureCellWithEvent(event:Event){
    
        dispatch_async(dispatch_get_main_queue(), {
            
            Util.loadImageFromStringUrl(self.imgPicture, strUrl: event.thumbnail!)
        })
        
    }
    
}
