//
//  EventsTasks.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 24/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit
import Alamofire
import MD5
import CoreData

class EventsTasks {
    
    private var eventParser = EventsParser()
    private let timeStemp:String!
    private var hash:String!
    private var appDelegate:AppDelegate!
    private var managedcontext: NSManagedObjectContext!
    
    init(){
        timeStemp = "\(NSDate().timeIntervalSince1970 * 1000)"
        hash = timeStemp+PRIVATE_KEY+PUBLIC_KEY
        hash = hash.md5Hash()
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedcontext = appDelegate.managedObjectContext
    }
    
    func retrieveEventsByCharactersHandler(characterId: String, handler:(status: Bool, result: [Event]?, error: ErrorType!) -> Void ) {
        
        let parameters:[String : AnyObject]!
        parameters = ["apikey": PUBLIC_KEY, "ts":timeStemp, "hash":hash]
        
        let urlRequests = UrlRequests()
        let url = NSURL(string: urlRequests.urlApiRetrieveEvents(characterId))
        
        Alamofire.request(.GET, url!, parameters: parameters).responseJSON { (response) -> Void in
            
            if(response.1?.statusCode == 200){
                
                let data = response.2.value!["data"] as! Dictionary<String,AnyObject>
                let retults = data["results"] as! NSArray
                
                let arrayEvents = self.eventParser.parserEventObject(retults as [AnyObject])
                
                handler(status: true, result: arrayEvents, error: nil)
                
            }else{
                
                handler(status: false, result: nil, error: response.2.error)
                
            }
            
        }
        
    }
    
    func getEventsByCharacterId(characterId:String, completion:(status:Bool, events:[Event])-> Void) {
        
        let fetchRequest = NSFetchRequest(entityName: "Event")
        let predicate = NSPredicate(format: "character.id == \(characterId)" )
        
        fetchRequest.predicate = predicate
        
        do{
            let events = try self.managedcontext!.executeFetchRequest(fetchRequest) as! [Event]
            
            if events.count > 0 {
                
                completion(status: true, events:events )
                
            }else{
                
                completion(status:false, events: events)
                
            }
            
        }catch{
            completion(status: false, events: [Event]())
            
        }
        
    }
    
}
