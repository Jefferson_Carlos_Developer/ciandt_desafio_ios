//
//  FavoritesTableViewCell.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 26/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit

class FavoritesTableViewCell: UITableViewCell {

    //MARK: - @IBOutlets
    @IBOutlet weak var imgPicture: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: - Functions
    func configureCellWithFavoritCharacter(character:Character){
        
        self.imgPicture.image = nil
        self.lblName.text = character.name
        self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        dispatch_async(dispatch_get_main_queue(), {
            
            Util.loadImageFromStringUrl(self.imgPicture, strUrl: character.thumbnail!)
        })
        
    }


}
