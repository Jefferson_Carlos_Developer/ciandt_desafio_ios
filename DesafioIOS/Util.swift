//
//  Util.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 12/04/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit
import AlamofireImage

class Util {
    
    static func loadImageFromStringUrl(imageView:UIImageView, strUrl:String){
        
        imageView.af_setImageWithURL(
            
            NSURL(string: strUrl)!,
            placeholderImage: UIImage(named:""),
            filter: nil,
            imageTransition: .CrossDissolve(1.0),
            completion: { response in }
        )
        
    }
    
    static func showAlert(vc:UIViewController, title: String, message:String){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        
        alertController.addAction(defaultAction)
        
        vc.presentViewController(alertController, animated: true, completion: nil)
    }
    
}
