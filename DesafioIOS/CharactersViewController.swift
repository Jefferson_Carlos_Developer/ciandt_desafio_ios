//
//  CharactersViewController.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 23/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit
import CoreData
import JGProgressHUD

class CharactersViewController: UIViewController{
    
    //MARK: - Properties
    private let charactersTask = CharactersTask()
    private var arrayCharacters = [Character]()
    private var offset:Int!
    private var appDelegate:AppDelegate!
    private var managedcontext: NSManagedObjectContext!
    private var tiler:NSTimer!
    private var totalCharacters = 0
    let HUD:JGProgressHUD = JGProgressHUD(style: .Dark)
    
    //MARK: - @IBOutlets
    @IBOutlet weak var sbSearch: UISearchBar!
    @IBOutlet weak var tvHeroes: UITableView!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        
        self.appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedcontext = appDelegate.managedObjectContext
        self.setDelegates()
        self.offset = 1
        self.tvHeroes.tableFooterView = UIView()
        self.getCharacters()

    }
    
    override func viewWillAppear(animated: Bool) {
        self.arrayCharacters = self.charactersTask.orderFavorites(self.arrayCharacters)
        self.tvHeroes.reloadData()
    }
    
    //MARK: - Functions
    func setDelegates(){
        self.tvHeroes.delegate = self
        self.tvHeroes.dataSource = self
        self.sbSearch.delegate = self
    }
    
    func getCharactersByName(name:String){
        
        self.sbSearch.resignFirstResponder()
        self.HUD.showInView(self.view)
        charactersTask.retrieveCharactersHandler(name) { (status, result, error) in
            
            self.HUD.dismiss()
            
            if(error == nil){
                
                if (status == true) {
                    self.arrayCharacters = result!
                    self.arrayCharacters = self.charactersTask.orderFavorites(self.arrayCharacters)
                    self.tvHeroes.reloadData()
                    
                }
                
            }
        }

    }
    
    func getCharacters(){
        
        self.sbSearch.resignFirstResponder()
        self.HUD.showInView(self.view)
        
        charactersTask.retrieveCharactersHandler(self.offset) { (total, status, result, error) in
            
            self.HUD.dismiss()
            
            if(error == nil){
                
                if (status == true) {
                    
                    self.totalCharacters = total
                    for character in result!{
                        self.arrayCharacters.append(character)
                    }
                    
                    self.arrayCharacters = self.charactersTask.orderFavorites(self.arrayCharacters)
                    self.tvHeroes.reloadData()
                    
                }
                
            }else{
                
            }
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "segueDetails"){
            
            let characterDetailsViewController = segue.destinationViewController as! CharacterDetailsViewController
            let index = self.tvHeroes.indexPathForSelectedRow?.row
            characterDetailsViewController.character = self.arrayCharacters[index!]
            characterDetailsViewController.openDetailFrom = CharacterDetailsViewController.OpenDetailFrom.Chacteres
            
        }
        
    }

}

//MARK: - UIScrollViewDelegate
extension CharactersViewController:UIScrollViewDelegate{
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.sbSearch.text = ""
        self.sbSearch.resignFirstResponder()
    }
}

//MARK: - UISearchBarDelegate
extension CharactersViewController:UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.sbSearch.resignFirstResponder()
        self.getCharactersByName(self.sbSearch.text!)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.sbSearch.resignFirstResponder()
            self.offset = 0
            self.arrayCharacters = [Character]()
            self.getCharacters()
        }
    }

}

//MARK: - UITableView Delegate & DataSource
extension CharactersViewController:UITableViewDelegate, UITableViewDataSource{

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayCharacters.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "cellCharacter"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! CharactersTableViewCell
        let character = self.arrayCharacters[indexPath.row]
        
        cell.configureCellWithCharacter(character)
        
        if (self.arrayCharacters.count < self.totalCharacters  &&
            indexPath.row+1 == self.arrayCharacters.count &&
            self.sbSearch.text == "" ){
            
            self.offset = self.offset+100
            self.getCharacters()
            
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.sbSearch.resignFirstResponder()
        self.performSegueWithIdentifier("segueDetails", sender: nil)
    }
    
}
