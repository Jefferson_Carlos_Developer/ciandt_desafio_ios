//
//  FavoritesViewController.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 24/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit
import CoreData

class FavoritesViewController: UIViewController, UISearchBarDelegate {
    
    //MARK: - Properties
    private var appDelegate:AppDelegate!
    private var managedcontext: NSManagedObjectContext!
    private var arrayHeroes = [Character]()
    private let charactersTask = CharactersTask()
    
    //MARK: - @IBOutlets
    @IBOutlet weak var tvFavoritesCharacters: UITableView!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        
        self.appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedcontext = appDelegate.managedObjectContext
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.getFavorites()
    }

    //MARK: - @IBActions
    @IBAction func actionClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - Functions
    func getFavorites(){
        
        charactersTask.getAllFavorites { (status, characters) in
            
            if(status == true){
                
                self.arrayHeroes = characters
                self.tvFavoritesCharacters.reloadData()
                
                if(self.arrayHeroes.count == 0){
                    Util.showAlert(self, title: "Desafio", message: "You have no favorites.")
                }
                
                
            }else{
            
            }
        }
    }
    
    func setDelegates(){
        self.tvFavoritesCharacters.delegate = self
        self.tvFavoritesCharacters.dataSource = self
    }
    
    //MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "segueDetails"){
            
            let characterDetailsViewController = segue.destinationViewController as! CharacterDetailsViewController
            let index = self.tvFavoritesCharacters.indexPathForSelectedRow?.row
            characterDetailsViewController.character = self.arrayHeroes[index!]
            characterDetailsViewController.openDetailFrom = CharacterDetailsViewController.OpenDetailFrom.Favorites
            
        }
        
    }
    
}

//MARK: - UITableView
extension FavoritesViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayHeroes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier = "cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier) as!FavoritesTableViewCell
        
        let character = self.arrayHeroes[indexPath.row] as Character
        
        cell.configureCellWithFavoritCharacter(character)
        
        if(character.thumbnail != ""){
            Util.loadImageFromStringUrl(cell.imgPicture, strUrl: character.thumbnail!)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("segueDetails", sender: nil)
    }
    
}
