//
//  CharactersTableViewCell.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 24/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit

class CharactersTableViewCell: UITableViewCell {

    //MARK: - @IBOutlets
    @IBOutlet weak var imgPicture: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgFavorite: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Functions
    func configureCellWithCharacter(character:Character){
        
        self.imgPicture.image = nil
        self.lblName.text = character.name
        self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
       
        if(character.isFavorite == true){
            self.imgFavorite.hidden = false
        }else{
            self.imgFavorite.hidden = true
        }
        
        dispatch_async(dispatch_get_main_queue(), {
           
            Util.loadImageFromStringUrl(self.imgPicture, strUrl: character.thumbnail!)
        })
    
    }
    
}
