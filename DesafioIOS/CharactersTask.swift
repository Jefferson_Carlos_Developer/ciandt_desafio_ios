//
//  CharactersTask.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 23/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit
import Alamofire
import MD5
import CoreData

class CharactersTask {
    
    //MARK: - Properties
    private var characterParser = CharacterParser()
    private var eventTasks = EventsTasks()
    private let timeStemp:String!
    private var hash:String!
    private var appDelegate:AppDelegate!
    private var managedcontext: NSManagedObjectContext!
    
    //MARK: - Inicialization
    init(){
        timeStemp = "\(NSDate().timeIntervalSince1970 * 1000)"
        hash = timeStemp+PRIVATE_KEY+PUBLIC_KEY
        hash = hash.md5Hash()
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedcontext = appDelegate.managedObjectContext
    }
    
    //MARK: - Functions
    func retrieveCharactersHandler(name: String, handler:(status: Bool, result: [Character]?, error: ErrorType!) -> Void ) {
        
        let parameters:[String : AnyObject]!
        parameters = ["apikey": PUBLIC_KEY, "nameStartsWith": name, "ts":timeStemp, "hash":hash]
        
        let urlRequests = UrlRequests()
        let url = NSURL(string: urlRequests.urlApiRetrieveCharacters())
        
        Alamofire.request(.GET, url!, parameters: parameters).responseJSON { (response) -> Void in
            
            if(response.1?.statusCode == 200){

                let data = response.2.value!["data"] as! Dictionary<String,AnyObject>
                let retults = data["results"] as! NSArray
                
                let arrayCharacters = self.characterParser.parserCharacterObject(retults as [AnyObject])
                
                for character in arrayCharacters{

                    self.checkCharacterIsFavoriteById(character.id!, completion: { (isFavorite) in
                        
                        character.isFavorite = isFavorite
                        
                    })
                }

                handler(status: true, result: arrayCharacters, error: nil)
                
            }else{
                
                handler(status: false, result: nil, error: response.2.error)
                
            }
            
        }
        
    }
    
    func retrieveCharactersHandler(offset:Int, handler:(total: Int, status: Bool, result: [Character]?, error: ErrorType!) -> Void ) {
        
        let parameters:[String : AnyObject]!
        parameters = ["apikey": PUBLIC_KEY, "limit":"100", "offset":offset, "ts":timeStemp, "hash":hash]
        
        let urlRequests = UrlRequests()
        let url = NSURL(string: urlRequests.urlApiRetrieveCharacters())
        
        Alamofire.request(.GET, url!, parameters: parameters).responseJSON { (response) -> Void in
            
            if(response.1?.statusCode == 200){
                
                let data = response.2.value!["data"] as! Dictionary<String,AnyObject>
                let total = data["total"] as! Int
                let retults = data["results"] as! NSArray
                
                let arrayCharacters = self.characterParser.parserCharacterObject(retults as! [Character])
                
                for character in arrayCharacters{
                    
                    self.checkCharacterIsFavoriteById(character.id!, completion: { (isFavorite) in
                        
                        character.isFavorite = isFavorite
                        
                    })
                }
                
                handler(total:total, status: true, result: arrayCharacters, error: nil)
                
            }else{
                
                handler(total:0, status: false, result: nil, error: response.2.error)
                
            }
            
        }
        
    }
    
    func orderFavorites(characters:[Character]) -> [Character] {
        
        var arrayCharacters = characters
        var arrayFavoritesCharacters = [Character]()
        var arrayCommonCharacters = [Character]()
        
        for character in arrayCharacters{
            
            self.checkCharacterIsFavoriteById(character.id!, completion: { (isFavorite) in
                
                character.isFavorite = isFavorite
                
                if(character.isFavorite == true){
                    arrayFavoritesCharacters.append(character)
                }else{
                    arrayCommonCharacters.append(character)
                }
                
            })
        }
        
        arrayCharacters.removeAll()
        arrayCharacters.appendContentsOf(arrayFavoritesCharacters)
        arrayCharacters.appendContentsOf(arrayCommonCharacters)
        
        return arrayCharacters

    }
    
    func removeCharacterFromFavoritesByCharacterId(characterId:String, completion:(status:Bool) -> Void) {
        
        let fetchRequest = NSFetchRequest(entityName: "Character")
        let predicate = NSPredicate(format: "id == \(characterId)" )
        
        fetchRequest.predicate = predicate
        
        do{
            let fetchResults = try self.managedcontext!.executeFetchRequest(fetchRequest) as! [Character]
            
            if fetchResults.count > 0 {
                
                for character in fetchResults{
                    
                    if(character.id! == character.id!){
                        
                        self.managedcontext.deleteObject(character)
                        
                        do{
                            try self.managedcontext.save()
                            print("Saved")
                            completion(status: true)
                            break
                            
                        }catch{
                            completion(status: true)
                            print("Error")
                            
                        }
                        
                    }
                }
                
            }else{
                
            }
            
        }catch{
            
        }
        
    }
    
    func addCharacterToFavorites(character:Character, completion:(status:Bool) -> Void) {
        
        let entityDescription = NSEntityDescription.entityForName("Character",inManagedObjectContext: self.managedcontext)
        
        let myCharacter = Character(entity: entityDescription!,insertIntoManagedObjectContext: self.managedcontext) as Character
        
        myCharacter.id = character.id
        myCharacter.name = character.name
        myCharacter.description_detail = character.description_detail
        myCharacter.thumbnail = character.thumbnail
        myCharacter.isFavorite = true
        
        self.eventTasks.retrieveEventsByCharactersHandler(myCharacter.id!, handler: { (status, result, error) in
            if(status == true){
                
                if(result!.count > 0){
                    
                    let entityDescription = NSEntityDescription.entityForName("Event", inManagedObjectContext: self.managedcontext)
                    let orderedSet = NSMutableOrderedSet()
                    
                    for event in result!{
                        let myEvent = Event(entity: entityDescription!,insertIntoManagedObjectContext: self.managedcontext) as Event
                        myEvent.id = event.id
                        myEvent.thumbnail = event.thumbnail
                        myEvent.character = myCharacter
                        myEvent.title = event.title
                        myEvent.descrption_detail = event.descrption_detail
                        
                        orderedSet.addObject(myEvent)
                    }
                    
                    myCharacter.events = orderedSet
                }
                
                do{
                    try myCharacter.managedObjectContext?.save()
                    
                    completion(status: true)
                    
                    print("Saved")
                    
                }
                catch{
                    completion(status: false)
                    
                    print("Error")
                }

                
            }
        })
        
        
    }
    
    func checkCharacterExists(character:Character, completion:(exists:Bool)-> Void) {
        
        let fetchRequest = NSFetchRequest(entityName: "Character")
        let predicate = NSPredicate(format: "id == \(character.id!)" )
        
        fetchRequest.predicate = predicate
        
        do{
            let fetchResults = try self.managedcontext!.executeFetchRequest(fetchRequest) as! [Character]
            
            if fetchResults.count > 0 {
                completion(exists: true)
            }else{
                completion(exists: false)
            }
            
        }catch{
            completion(exists: false)
            
        }
        
    }
    
    func getAllFavorites(completion:(status:Bool, characters:[Character]) -> Void){
        
        let fetchRequest = NSFetchRequest(entityName: "Character")
        
        do{
            let favoriteCharacters = try managedcontext.executeFetchRequest(fetchRequest) as! [Character]
            completion(status: true, characters: favoriteCharacters)
            
        }
        catch{
            completion(status: false, characters: [Character]())
        }
    }
    
    func checkCharacterIsFavoriteById(characterId:String, completion:(isFavorite:Bool) -> Void) {
        
        let fetchRequest = NSFetchRequest(entityName: "Character")
        let predicate = NSPredicate(format: "id == \(characterId)" )

        fetchRequest.predicate = predicate
        
        do{
           
            let fetchResults = try self.managedcontext!.executeFetchRequest(fetchRequest)
            
            if(fetchResults.count > 0){
                completion(isFavorite: true)
            }else{
                completion(isFavorite: false)
            }
            
        }catch{
            completion(isFavorite: false)
        }
        
    }
    
}
