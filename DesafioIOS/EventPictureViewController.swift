//
//  EventPictureViewController.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 26/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit

class EventPictureViewController: UIViewController {

    //MARK: - Properties
    var urlImage:String!
    
    //MARK: - @IBOutlets
    @IBOutlet weak var imgPicture: UIImageView!

    //MARK: - LifeClycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setImage()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Functions
    func setImage(){
        dispatch_async(dispatch_get_main_queue(), {
            
            Util.loadImageFromStringUrl(self.imgPicture, strUrl: self.urlImage)
        })
    }


}
