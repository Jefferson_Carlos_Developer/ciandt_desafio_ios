//
//  Character+CoreDataProperties.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 26/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Character {

    @NSManaged var id: String?
    @NSManaged var thumbnail: String?
    @NSManaged var description_detail: String?
    @NSManaged var isFavorite: NSNumber?
    @NSManaged var name: String?
    @NSManaged var events: NSOrderedSet?

}
