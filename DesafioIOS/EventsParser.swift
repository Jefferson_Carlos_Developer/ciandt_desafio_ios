//
//  EventsParser.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 24/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit
import CoreData

class EventsParser {

    var events = [Event]()
    
    func parserEventObject(results: [AnyObject] ) -> [Event]{
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedcontext = appDelegate.managedObjectContext
        let entity = NSEntityDescription.entityForName("Event", inManagedObjectContext: managedcontext)
        
        for event in results{
            
            let myEvent = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: nil) as! Event
            
            if let id = event["id"]!{
                myEvent.id = String(id)
            }
            
            if let description = event["description"]!{
                myEvent.descrption_detail = description as? String
            }
            
            if let title = event["title"]!{
                myEvent.title = title as? String
            }
            
            if let thumbnail = event["thumbnail"]!{
                
                var thumbnailPath = ""
                
                if let path = thumbnail["path"]!{
                    thumbnailPath = path as! String
                }
                
                if let thumbnailExtension = thumbnail["extension"]!{
                    thumbnailPath = "\(thumbnailPath).\(thumbnailExtension)"
                }
                
                myEvent.thumbnail = String(thumbnailPath)
            }

            events.append(myEvent)
            
        }
        
        return events
    }

    
}
