//
//  EventsViewController.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 24/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit
import JGProgressHUD

class EventsViewController: UIViewController {

    //MARK: - Properties
    private let eventTasks = EventsTasks()
    private var arrayEvents = [Event]()
    private var offset:Int!
    private var selectedIndex = 0
    
    var characterId:String!
    var openEventsFrom:CharacterDetailsViewController.OpenDetailFrom!
    let HUD:JGProgressHUD = JGProgressHUD(style: .Dark)

    //MARK: - @IBOutlets
    @IBOutlet weak var cvPictures: UICollectionView!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDelegates()
        self.getEvents()
        self.offset = 0
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Functions
    func getEvents(){
        
        self.HUD.showInView(self.view)
        
        if(self.openEventsFrom == CharacterDetailsViewController.OpenDetailFrom.Chacteres){

            self.eventTasks.retrieveEventsByCharactersHandler(self.characterId) { (status, result, error) in
                
                self.HUD.dismiss()
                
                if(error == nil){
                    
                    if (status == true) {
                        
                        self.arrayEvents = result!
                        self.cvPictures.reloadData()
                        
                        if(self.arrayEvents.count == 0){
                            Util.showAlert(self, title: "Desafio", message: "There's no events!")
                        }
                        
                    }
                    
                }
            }
            
        }else{
            self.eventTasks.getEventsByCharacterId(self.characterId, completion: { (status, events) in
                self.HUD.dismiss()

                if(status == true){
                    self.arrayEvents = events
                    self.cvPictures.reloadData()
                    
                    if(self.arrayEvents.count == 0){
                        Util.showAlert(self, title: "Desafio", message: "There's no events!")
                    }

                }
                
            })
    
        }

    }
    
    func setDelegates(){
        self.cvPictures.delegate = self
        self.cvPictures.dataSource = self
    }

    //MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "openImage"){

            let eventPictureViewCOntroller = segue.destinationViewController as! EventPictureViewController
            eventPictureViewCOntroller.urlImage = self.arrayEvents[self.selectedIndex].thumbnail
            
        }
    }
    
}

//MARK: - UICollectionView
extension EventsViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayEvents.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.selectedIndex = indexPath.row
        self.performSegueWithIdentifier("openImage", sender: nil)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let event = self.arrayEvents[indexPath.row]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellPicture", forIndexPath: indexPath) as! EventsCollectionViewCell
        
        cell.configureCellWithEvent(event)
        
        // Configure the cell
        return cell
    
    }
    
}
