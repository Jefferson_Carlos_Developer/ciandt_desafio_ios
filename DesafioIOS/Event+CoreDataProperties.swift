//
//  Event+CoreDataProperties.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 26/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Event {

    @NSManaged var id: String?
    @NSManaged var thumbnail: String?
    @NSManaged var descrption_detail: String?
    @NSManaged var title: String?
    @NSManaged var character: Character?

}
