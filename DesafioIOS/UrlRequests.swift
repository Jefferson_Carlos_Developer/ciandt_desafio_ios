//
//  UrlRequests.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 23/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import Foundation

let PUBLIC_KEY = "2ec8ca4f057f4345a9fe5d204a21fbc3"
let PRIVATE_KEY = "7622631c17aaef4134a29f89f3d9d888f07a791f"

class UrlRequests {

    private let baseUrl = "http://gateway.marvel.com:80"
    private let charactersUrl = "/v1/public/characters"
    
    private func getBaseUrl() -> String{
        return baseUrl
    }

    private func getBaseUrlWithSufix(sufix:String) -> String{
        return getBaseUrl()+sufix
    }

    func urlApiRetrieveCharacters() -> String{
        return self.getBaseUrlWithSufix(charactersUrl)
    }
    
    func urlApiRetrieveEvents(characterId:String) -> String{
        return self.getBaseUrlWithSufix("\(charactersUrl)/\(characterId)/events")
    }


}