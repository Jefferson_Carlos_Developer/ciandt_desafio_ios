//
//  CharacterParser.swift
//  DesafioIOS
//
//  Created by jeffersoncsilva on 23/05/17.
//  Copyright © 2017 Jefferson Carlos. All rights reserved.
//

import UIKit
import CoreData

class CharacterParser: NSObject {
    
    func parserCharacterObject(results: [AnyObject] ) -> [Character]{
        
        var characters = [Character]()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedcontext = appDelegate.managedObjectContext
        let entity = NSEntityDescription.entityForName("Character", inManagedObjectContext: managedcontext)
        
        for character in results{
            
            let myCharacter = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: nil) as! Character
            
            if let id = character["id"]!{
                myCharacter.id = String(id)
            }
            
            if let name = character["name"]!{
                myCharacter.name = String(name)
            }
            
            if let thumbnail = character["thumbnail"]!{
                
                var thumbnailPath = ""
                
                if let path = thumbnail["path"]!{
                    thumbnailPath = path as! String
                }
                
                if let thumbnailExtension = thumbnail["extension"]!{
                    thumbnailPath = "\(thumbnailPath).\(thumbnailExtension)"
                }
                
                myCharacter.thumbnail = String(thumbnailPath)
                
            }
            
            if let description = character["description"]!{
                myCharacter.description_detail = description as? String
            }
            
            if let isFavorite = character["isFavorite"]!{
                myCharacter.isFavorite = isFavorite as? Bool
            }
                        
            characters.append(myCharacter)
        
        }
        
        return characters
    }
    
}
